﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Request.aspx.cs" Inherits="Request" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Chapter 6: Happy Earwig</title>
        <link href="Styles/Main.css" rel="stylesheet" type="text/css"/>
        <link href="Styles/Request.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <header>
        <div class="setWidth">
            <asp:Image ID="imgHappyEarwigMotelImage" CssClass="center" runat="server" DescriptionUrl="~/Images/The_Happy_Earwig_Motel.png" ImageUrl="~/Images/The_Happy_Earwig_Motel.png" />
        </div>
        <h1>
            
            The Happy Earwig Motel</h1>
        <h2>Our crawl-space now body-free!</h2>
    </header>
    <section>
        <form id="form1" runat="server">
            <h1>Reservation Request</h1>
            <h2>Request data</h2>
            <label class="label">Arrival</label>
            <asp:TextBox ID="tbArrivalDate" runat="server" TextMode="Date" TabIndex="1"></asp:TextBox>
            <br/>
            <label class="label">Departure</label>
            <asp:TextBox ID="tbDeparture" runat="server" TextMode="Date" TabIndex="2"></asp:TextBox>
            <br/>
            <label class="label">Number of adults</label>
            <asp:DropDownList ID="ddlNumberOfAdults" CssClass="ddlWidth" runat="server" TabIndex="3">
            </asp:DropDownList>
            <br/>

            <label class="label">Bed type</label>
            <asp:RadioButton ID="rbKing" runat="server" GroupName="BedType" Text="King" TabIndex="4"/>
            &nbsp;
            <asp:RadioButton ID="rbTwoQueens" runat="server" GroupName="BedType" Text="Two Queens" TabIndex="4"/>
            &nbsp;
            <asp:RadioButton ID="rbOneQueen" runat="server" GroupName="BedType" Text="One Queen" TabIndex="4"/>
            <br/>

            <h2>Special requests</h2>
            <asp:TextBox ID="taSpecialRequests" runat="server" TextMode="MultiLine" TabIndex="5"></asp:TextBox>
            <br/>

            <h2>Contact information</h2>
            <label class="label">First name</label>
            <asp:TextBox ID="tbFirstName" runat="server" TabIndex="6"></asp:TextBox>
            <br/>
            <label class="label">Last name</label>
            <asp:TextBox ID="tbLastName" runat="server" TabIndex="7"></asp:TextBox>
            <br/>
            <label class="label">Email address</label>
            <asp:TextBox ID="tbEmail" runat="server" TextMode="Email" TabIndex="8"></asp:TextBox>
            <br/>
            <label class="label">Telephone number</label>
            <asp:TextBox ID="tbTelephone" runat="server" TextMode="Phone" TabIndex="9"></asp:TextBox>
            <br/>
            <label class="label">Preferred method</label>

            <asp:DropDownList ID="ddlPreferedMethod" CssClass="ddlWidth" runat="server" TabIndex="10">
            </asp:DropDownList>
            <br/>

            <label class="label">&nbsp;</label>
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="button" TabIndex="11" OnClick="btnSubmit_Click"/>&nbsp;
            <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="button" OnClick="btnClear_Click" TabIndex="12"/><br/>
            <p>
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </p>
        </form>
    </section>
    <footer>
        <p>&copy; 2015, Happy Earwig Motel</p>
    </footer>
    </body>
</html>
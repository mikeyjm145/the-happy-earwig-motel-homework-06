﻿
using System;
using System.Web.UI.WebControls;

/// <summary>
/// Creates a request for a love hotel room reservation.
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
public partial class Request : System.Web.UI.Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.tbArrivalDate.Text = DateTime.UtcNow.AddDays(-1).ToString("yyyy-MM-dd");
        this.tbDeparture.Text = DateTime.UtcNow.ToString("yyyy-MM-dd");
        this.rbKing.Checked = true;
        this.rbOneQueen.Checked = false;
        this.rbTwoQueens.Checked = false;
        this.tbArrivalDate.Focus();

        for (int i = 1; i <= 4; i++)
        {
            this.ddlNumberOfAdults.Items.Add(new ListItem(i.ToString(),i.ToString()));
        }

        this.ddlPreferedMethod.Items.Clear();
        this.ddlPreferedMethod.Items.Add(new ListItem("Email", "Email"));
        this.ddlPreferedMethod.Items.Add(new ListItem("Phone", "Phone"));
    }

    /// <summary>
    /// Handles the Click event of the btnClear control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnClear_Click(object sender, EventArgs e)
    {
        this.tbArrivalDate.Text = DateTime.UtcNow.AddDays(-1).ToString("yyyy-MM-dd");
        this.tbDeparture.Text = DateTime.UtcNow.ToString("yyyy-MM-dd");
        this.rbKing.Checked = true;
        this.tbFirstName.Text = "";
        this.tbLastName.Text = "";
        this.tbTelephone.Text = "";
        this.ddlNumberOfAdults.SelectedIndex = 0;
        this.ddlPreferedMethod.SelectedIndex = 0;
    }

    /// <summary>
    /// Handles the Click event of the btnSubmit control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        this.lblMessage.Text = "Thank you for your request. <br> We will get back to you within 24 hours.";
    }
}